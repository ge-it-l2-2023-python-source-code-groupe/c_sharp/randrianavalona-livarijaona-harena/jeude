namespace ClassJeux;
public partial class Manche
{
    public void AfficherHistorique()
    {
        AfficherDateEtHeure();
        for (int i = 0; i < nombreJoueurs; i++)
        {
            historiqueIndividuels[i] = ($"{nomIndividuels[i]}\nson socre est : {scoreIndividuels[i]}\nil a eu {faceIndividuels[i]} lors du lancement");
            Console.WriteLine(historiqueIndividuels[i]);
        }
       
    }
    public void AfficherDateEtHeure()
        {
            DateTime heureDuJeu = DateTime.Now;
            string format = "dd/MM/yyyy HH:mm:ss"; // Format de date et heure

            string dateHeureFormatee = heureDuJeu.ToString(format);

            Console.WriteLine($"Date et heure du jeu : {dateHeureFormatee}");
        }

}