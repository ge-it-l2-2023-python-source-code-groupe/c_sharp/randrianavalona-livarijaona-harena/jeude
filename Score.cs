namespace ClassJeux;
public partial class Manche
{
    public void AfficherScore()
    {
        int i = 0;
        while (i<nombreJoueurs)
        {
            Console.WriteLine($"Score de {nomIndividuels[i]} : {scoreIndividuels[i]} points");
            i++;
        }
        
        
    }
    public void AttributionScore()
{
    faceIndividuels = faceString.Split(';');
    scoreIndividuels = scoresdesJoueurs.Split(';');
    int meilleurJet = 0;
    int faceActuelle = 0;
    // Parcourir les faces des joueurs pour trouver le meilleur tirage
    for (int i = 0; i < nombreJoueurs; i++)
    {
        faceActuelle = Int32.Parse(faceIndividuels[i]);

        // Mettre à jour le meilleur tirage si la face actuelle est meilleure
        if (meilleurJet < faceActuelle)
        {
            meilleurJet = faceActuelle;
        }
    }

    // Parcourir à nouveau les faces pour attribuer les points en fonction du meilleur tirage
    
    
    for (int i = 0; i < nombreJoueurs; i++)
    {
        faceActuelle = Int32.Parse(faceIndividuels[i]);

        // Comparer la face actuelle avec le meilleur tirage
        if (faceActuelle == 6 && nombreSix == 1)
        {
            // Le joueur a le meilleur tirage, attribuer 2 points
            scoreIndividuels[i] = "2";
            
            
        }
        
        else if (faceActuelle == 6)
        {
            scoreIndividuels[i] = "1";
        }
        else
        {
            // Le joueur n'a pas le meilleur tirage, ne gagne pas de points
            scoreIndividuels[i] = "0";
        }
    }
}
}